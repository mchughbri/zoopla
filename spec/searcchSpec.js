'use strict';

const request = require("request");
const base = 'http://localhost:3000';

describe("Search for property", () => {
    describe("GET /search", () => {
        it("returns status code 200", () => {
            request.get(base + '/search', (error, response, body) => {
                expect(response.statusCode).toBe(200);
                done();
            });
        });
    });
});

describe("Results", () => {
    describe("GET /results", () => {
        it("returns status code 200", () => {
            request.get(base + '/results', (error, response, body) => {
                expect(response.statusCode).toBe(200);
                done();
            });
        });
    });
});
