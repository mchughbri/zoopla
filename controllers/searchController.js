'use strict';

// Mimic API for purpose of test
const data = require('../data/data'); 
const request = require('request');
var rp = require('request-promise');


// Render Search View
exports.render_search = (req, res, next) => {
    res.render('search', { 
        title: 'Search',
        subtitle: 'Search for houses and flats for sale across the uk' 
    });
};

let render_results = (res, results) => {
    res.render('results', { 
        title: 'Search Results', 
        results: results,
        showResults: results.result_count > 0,
        helpers: {
          formatCurrency: (price) => price.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")
        }
    })
};

// Render results in results view
exports.get_results = (req, res, next) => {
    let query = req.body.q;
    let endpoint = "http://localhost:3000/api/search/" + query.toUpperCase();
    let options = {
        uri: endpoint,
        json: true
    };
    rp(options)
        .then( results => render_results(res, results ) )
        .catch( error => console.log("Error:", error) )
}; 

// Return API JSON
exports.get_api_properties = (req, res, next) => {
    let query = req.params.q;
    let results =  (query === data.area) ? data : {"status": "No results found", "result_count": 0};
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(results)); 
};

