'use strict';

const express = require('express');
const router = express.Router();
const searchController = require('../controllers/searchController');

/* Home */
router.get('/', (req, res) => res.redirect('/search') );

/* Search */
router.get('/search', searchController.render_search);

/* Results */
router.post('/results', searchController.get_results);

/* API Search */
router.get('/api/search/:q', searchController.get_api_properties);

module.exports = router;
